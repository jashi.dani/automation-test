# Automation Test

Automation test for front and backend using cypress


## Getting Started

run `npm install` and `npm run test`  then select:

* buy-product.spec.js (front-end)
* mail-validation.spec.js (back-end)

### Prerequisites

* Nodejs
* Cypress 

