context('Mail Validation', function (){
    it('Should verify that michael.lawson@reqres.in exist in page 2', function () {
       cy.request('GET','https://reqres.in/api/users?page=2')
       .then((response) => {
           const user = response.body.data.find(item => item.email === 'michael.lawson@reqres.in');
           expect(user).to.not.be.null;
           expect(user.id).to.equal(7);
           expect(user.email).to.equal("michael.lawson@reqres.in");
           expect(user.first_name).to.equal("Michael");
           expect(user.last_name).to.equal("Lawson");
           expect(user.avatar).to.equal("https://reqres.in/img/faces/7-image.jpg");
           
       })
    })
  })