context('Cloth Store', function (){
  it('Should buy a product', function () {
     cy.visit(Cypress.env('baseUrl'));
     cy.contains('Sign in').click();

     cy.login(Cypress.env('email'),Cypress.env('password'));     
     cy.addItemToCart(2);
     cy.checkoutCart();

     cy.get('.alert.alert-success').should('have.text', 'Your order on My Store is complete.');
    })
})