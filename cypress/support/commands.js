Cypress.Commands.add("login", (email, password) => {
    cy.get('#email').type(email)
    cy.get('#passwd').type(password)
    cy.get('#SubmitLogin').click()
});
Cypress.Commands.add("addItemToCart", (categoryIndex) => {
    cy.get('.menu-content > li').eq(categoryIndex).click()
    cy.get('.product_img_link').trigger('mouseover')
    cy.contains('Add to cart').click()
});

Cypress.Commands.add("checkoutCart", () => {
    cy.contains('Proceed to checkout').click()
    cy.get('a.btn.btn-default.standard-checkout.button-medium').click()  
    cy.get('button.btn.btn-default.button-medium').click()
    cy.get('[type="checkbox"]').check()
    cy.get('button.btn.btn-default.standard-checkout.button-medium').click() 
    cy.contains('Pay by check').click()  
    cy.get('button.button.btn.btn-default.button-medium').click() 
});

